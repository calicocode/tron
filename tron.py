# tron.py, a Tron clone. Players control cycles using arrow keys and WASD keys
# Shannon Chopson, ITEC 4250, Spring 2016
# Dr. Myungjae Kwak

import pygame, sys, random, time, copy
from pygame.locals import *

# Constants
FPS = 15
WINDOWWIDTH = 800
WINDOWHEIGHT = 600
CELLSIZE = 20
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
PURPLE    = (128,   0, 128)
AQUA      = (  0, 255, 255)
ORANGE    = (255, 127,  39)
BLACK     = (  0,   0,   0)
DARKGRAY  = ( 40,  40,  40)
BGCOLOR = BLACK
DEFAULTONE = 1 #Default index of cycle color for player 1
DEFAULTTWO = 2 #Default index of cycle color for player 2

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'
STOP = 'stop'

START = "Start"
ONEPLAYER = "1 Player"
TWOPLAYER = "2 Player"
SETTINGS = "Settings"

CYCLE = 0 #index of cycle in light trails
CYCLESIZE = 31 #height in pixels of cycle asset

class Cycle:

    def __init__(self, color, playerOne):
        self.color = color # one of the defined color constants
        self.playerOne = playerOne # Boolean flag 
        self.x = int(CELLWIDTH / 2) # grid x-coordinate of cycle

        if playerOne == True: # determine grid y-coordinate of cycle
            self.y = CELLHEIGHT - 1 # at bottom of arena
            self.direction = UP
        else:
            self.y = 0 # at top of arena
            self.direction = DOWN
        # list of dictionaries holds coordinates of light trails and cycle
        self.lightCoords = [{'x': self.x, 'y': self.y}] 
    
    def getColor(self):
        return self.color

    def getDirection(self):
        return self.direction

    def setDirection(self, direction):
        self.direction = direction

    def getLightTrails(self):
        # returns coordinates of light trail (includes cycle)
        return self.lightCoords

    def getCycleCoord(self):
        self.cyclex = self.lightCoords[CYCLE]['x']
        self.cycley = self.lightCoords[CYCLE]['y']
        return self.cyclex, self.cycley

    def deleteLightCoord(self, position):
        # deletes specified coordinate in light trail
        self.position = position
        del self.lightCoords[self.position]

    def updateLightTrail(self):
        if self.direction == UP:
            newCycle = {'x': self.lightCoords[CYCLE]['x'], 'y':
                        self.lightCoords[CYCLE]['y'] - 1}
        elif self.direction == DOWN:
            newCycle = {'x': self.lightCoords[CYCLE]['x'], 'y':
                        self.lightCoords[CYCLE]['y'] + 1}
        elif self.direction == LEFT:
            newCycle = {'x': self.lightCoords[CYCLE]['x'] - 1, 'y':
                        self.lightCoords[CYCLE]['y']}
        elif self.direction == RIGHT:
            newCycle = {'x': self.lightCoords[CYCLE]['x'] + 1, 'y':
                        self.lightCoords[CYCLE]['y']}
        self.lightCoords.insert(0, newCycle)

    def collisionDetection(self, player2): #returns true if collision occurred
        self.player2 = player2
        # checks for hitting the edge
        if (self.lightCoords[CYCLE]['x'] in (-1, CELLWIDTH)) or \
        (self.lightCoords[CYCLE]['y'] in (-1, CELLHEIGHT)):
            self.direction = STOP
            return True

        # checks for hitting itself
        for lightTrail in self.lightCoords[1:]:
            if (lightTrail['x'] == self.lightCoords[CYCLE]['x']) and \
            (lightTrail['y'] == self.lightCoords[CYCLE]['y']):
                self.direction = STOP
                return True

        # checks for hitting other player
        for otherTrail in self.player2.getLightTrails():
            if (otherTrail['x'] == self.lightCoords[CYCLE]['x']) and \
            (otherTrail['y'] == self.lightCoords[CYCLE]['y']):
                self.direction = STOP
                return True  

    def drawCycle(self, surface):
        self.surface = surface

        for coord in self.lightCoords[1:]:
            x = coord['x'] * CELLSIZE
            y = coord['y'] * CELLSIZE
            lightSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
            pygame.draw.rect(self.surface, self.color, lightSegmentRect)
        cycleCoords = (self.lightCoords[CYCLE]['x'] * CELLSIZE,
                       self.lightCoords[CYCLE]['y'] * CELLSIZE)
        # ensures cycle image is properly rotated for direction of travel
        if self.direction == RIGHT:
            cycleSurf = pygame.transform.rotate(CYCLEIMAGE, 90)
            cycleRect = cycleSurf.get_rect()
            cycleRect.topleft = cycleCoords
        elif self.direction == LEFT:
            cycleSurf = pygame.transform.rotate(CYCLEIMAGE, 270)
            cycleRect = cycleSurf.get_rect()
            cycleRect.topright = (self.lightCoords[1]['x'] * CELLSIZE,
                                  self.lightCoords[1]['y'] * CELLSIZE)
        else:
            cycleRect = CYCLEIMAGE.get_rect()
            cycleSurf = CYCLEIMAGE
            cycleRect.topleft = cycleCoords
        self.surface.blit(cycleSurf, cycleRect)
        


def main():

    global FPSCLOCK, DISPLAYSURF, BASICFONT, LOGOIMAGE, CYCLEIMAGE, COLORS

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_icon(pygame.image.load('Tron_icon.png'))
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 24)
    pygame.display.set_caption('Tron')

    # Load images
    LOGOIMAGE = pygame.image.load('Tron_logo.png')
    CYCLEIMAGE = pygame.image.load('cycle.png')
    gameMode = START
    COLORS = [WHITE, RED, GREEN, PURPLE, AQUA, ORANGE]
    color1, color2 = DEFAULTONE, DEFAULTTWO
    
    #returns game mode
    while True:
        if gameMode == START: #This returns SETTINGS, ONEPLAYER, or TWOPLAYER
            gameMode, color1, color2 = showStartScreen(color1, color2) 
        elif gameMode == SETTINGS:
            gameMode, color1, color2 = showSettingsScreen(color1, color2)
        elif gameMode in (ONEPLAYER, TWOPLAYER):
            gameMode, color1, color2 = runGame(gameMode, color1, color2)
        

def runGame(gameMode, color1, color2):
    gameMode, color1, color2 = gameMode, color1, color2
    #create cycles
    me = Cycle(COLORS[color1], True)
    you = Cycle(COLORS[color2], False)

    showCountdown()
    while True: # main game loop
        checkForQuit()
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if (event.key == K_LEFT) and (me.getDirection != RIGHT):
                    me.setDirection(LEFT)
                elif (event.key == K_RIGHT) and (me.getDirection != LEFT):
                    me.setDirection(RIGHT)
                elif (event.key == K_UP) and (me.getDirection != DOWN):
                    me.setDirection(UP)
                elif (event.key == K_DOWN) and (me.getDirection != UP):
                    me.setDirection(DOWN)
                elif gameMode == TWOPLAYER:
                    if (event.key == K_a) and (you.getDirection != RIGHT):
                        you.setDirection(LEFT)
                    elif (event.key == K_d) and (you.getDirection != LEFT):
                        you.setDirection(RIGHT)
                    elif (event.key == K_w) and (you.getDirection != DOWN):
                        you.setDirection(UP)
                    elif (event.key == K_s) and (you.getDirection != UP):
                        you.setDirection(DOWN)

        if gameMode == ONEPLAYER: #Rule-based AI to determine NPC's move
            x1, y1 = me.getCycleCoord()
            x2, y2 = you.getCycleCoord()
            distancex = x2 - x1
            distancey = y2 - y1
            #strategy code that tries to cut across the other player's line
            #of travel, hopefully causing them to crash
            if me.getDirection() == you.getDirection():
                if you.getDirection() == RIGHT:
                    if distancex > (abs(distancey) + 1):
                        if distancey < 0:
                            you.setDirection(DOWN)
                        else:
                            you.setDirection(UP)
                elif you.getDirection() == LEFT:
                    if abs(distancex) > (abs(distancey) + 1):
                        if distancey < 0:
                            you.setDirection(DOWN)
                        else:
                            you.setDirection(UP)
                elif you.getDirection() == UP:
                    if abs(distancey) > (abs(distancex) + 1):
                        if distancex > 0:
                            you.setDirection(LEFT)
                        else:
                            you.setDirection(RIGHT)
                elif you.getDirection() == DOWN:
                    if distancey > (abs(distancex) + 1):
                        if distancex < 0:
                            you.setDirection(LEFT)
                        else:
                            you.setDirection(RIGHT)
            you.updateLightTrail()
            
            directions = [UP, DOWN, LEFT, RIGHT]
            while True: # makes avoiding collisions a priority over strategy
                if you.collisionDetection(me) and (len(directions) > 0):
                    you.deleteLightCoord(CYCLE)
                    index = random.randint(0, len(directions) - 1)
                    you.setDirection(directions[index])
                    del directions[index]
                    you.updateLightTrail()
                else: #allows ai to lose if it runs out of valid options and must crash
                    break
                    
        me.updateLightTrail()
        if gameMode == TWOPLAYER:
            you.updateLightTrail() #to avoid updating trail twice in oneplayer mode

        if me.collisionDetection(you) or you.collisionDetection(me):
            #display game over screen
            gameMode, color1, color2 = showGameOverScreen()
            return gameMode, color1, color2
        
        DISPLAYSURF.fill(BGCOLOR)
        drawGrid()
        me.drawCycle(DISPLAYSURF)
        you.drawCycle(DISPLAYSURF)
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def showStartScreen(color1, color2):
    color1, color2 = color1, color2
    logoImage = pygame.transform.smoothscale(LOGOIMAGE, (WINDOWWIDTH, WINDOWHEIGHT))
    logoRect = logoImage.get_rect()

    #Buttons
    onePSurf = BASICFONT.render('1 Player', True, WHITE)
    onePRect = onePSurf.get_rect()
    onePRect.center = (int(WINDOWWIDTH / 2) - 60, int(WINDOWHEIGHT / 2) + 90)

    twoPSurf = BASICFONT.render('2 Player', True, WHITE)
    twoPRect = twoPSurf.get_rect()
    twoPRect.center = (int(WINDOWWIDTH / 2) + 60, int(WINDOWHEIGHT / 2) + 90)

    settingsSurf = BASICFONT.render('Settings', True, WHITE)
    settingsRect = settingsSurf.get_rect()
    settingsRect.center = (WINDOWWIDTH - 70, WINDOWHEIGHT - 20)

    gameMode = START
    while True:
        checkForQuit()
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONUP:
                mousex, mousey = event.pos
                if onePRect.collidepoint( (mousex, mousey) ):
                    gameMode = ONEPLAYER
                    return gameMode, color1, color2
                elif twoPRect.collidepoint( (mousex, mousey) ):
                    gameMode = TWOPLAYER
                    return gameMode, color1, color2
                elif settingsRect.collidepoint( (mousex, mousey) ):
                    gameMode = SETTINGS
                    return gameMode, color1, color2
        DISPLAYSURF.fill(BGCOLOR)
        DISPLAYSURF.blit(logoImage, logoRect)
        DISPLAYSURF.blit(onePSurf, onePRect)
        DISPLAYSURF.blit(twoPSurf, twoPRect)
        DISPLAYSURF.blit(settingsSurf, settingsRect)
        pygame.display.update()
        FPSCLOCK.tick(FPS)

def showSettingsScreen(color1, color2):
    color1, color2 = color1, color2
    BOXSIZE = CYCLESIZE
    left1 = int(WINDOWWIDTH / 4)
    left2 = int(3 * left1)
    top = int(BOXSIZE + 20)
    colorCoords = []

    #Buttons
    backSurf = BASICFONT.render('Back', True, WHITE)
    backRect = backSurf.get_rect()
    backRect.topleft = (30, int(WINDOWHEIGHT - BOXSIZE))

    p1TextSurf = BASICFONT.render('Player 1', True, WHITE)
    p1TextRect = p1TextSurf.get_rect()
    p1TextRect.center = (left1, BOXSIZE)

    p2TextSurf = BASICFONT.render('Player 2', True, WHITE)
    p2TextRect = p1TextSurf.get_rect()
    p2TextRect.center = (left2, BOXSIZE)
    # Two sets of rect objects provide collision detection for color selection.
    # The Player rects are two columns; the Color rects are rows.
    colorRects = []
    for i in range(len(COLORS)):
        colorCoords.append(top)
        colorRects.append(pygame.Rect(left1, colorCoords[i], int(left2 + BOXSIZE), BOXSIZE))
        top += int(10 + BOXSIZE)
    player1Rect = pygame.Rect(left1, colorCoords[0], BOXSIZE, colorCoords[-1])
    player2Rect = pygame.Rect(left2, colorCoords[0], BOXSIZE, colorCoords[-1])

    cycleRect1 = CYCLEIMAGE.get_rect()
    cycleRect2 = CYCLEIMAGE.get_rect()
    cycleRect1.right = left1 - 10
    cycleRect2.right = left2 - 10
    cycleRect1.top = (colorRects[color1]).top
    cycleRect2.top = (colorRects[color2]).top
    
    while True:
        checkForQuit()
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONUP:
                mousex, mousey = event.pos
                if player1Rect.collidepoint( (mousex, mousey) ):
                    for i in range(len(colorRects)):
                        if colorRects[i].collidepoint( (mousex, mousey) ):
                            color1 = i
                            cycleRect1.top = colorRects[i].top
                elif player2Rect.collidepoint( (mousex, mousey) ):
                    for i in range(len(colorRects)):
                        if colorRects[i].collidepoint( (mousex, mousey) ):
                            color2 = i
                            cycleRect2.top = colorRects[i].top
                elif backRect.collidepoint( (mousex, mousey) ):
                    gameMode = START
                    return gameMode, color1, color2
        DISPLAYSURF.fill(BGCOLOR)
        DISPLAYSURF.blit(backSurf, backRect)
        DISPLAYSURF.blit(p1TextSurf, p1TextRect)
        DISPLAYSURF.blit(p2TextSurf, p2TextRect)
        for i in range(len(COLORS)):
            pygame.draw.rect(DISPLAYSURF, COLORS[i], \
                             (left1, colorCoords[i], BOXSIZE, BOXSIZE))
            pygame.draw.rect(DISPLAYSURF, COLORS[i], \
                             (left2, colorCoords[i], BOXSIZE, BOXSIZE))
        DISPLAYSURF.blit(CYCLEIMAGE, cycleRect1)
        DISPLAYSURF.blit(CYCLEIMAGE, cycleRect2)
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def drawGrid():
    for x in range(0, WINDOWWIDTH, CELLSIZE): # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE): # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))

def checkForQuit():
    for event in pygame.event.get((QUIT, KEYUP)):
        if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
            terminate()

def terminate():
    pygame.quit()
    sys.exit()

def showCountdown():
    numberFont = pygame.font.Font('freesansbold.ttf', 400)
    numberSurfs = []
    numberRects = []
    for i in range(3, 0, -1):
        numberSurfs.append(numberFont.render(str(i), True, WHITE))
    while True:
        DISPLAYSURF.fill(BGCOLOR)
        for i in range(0, len(numberSurfs)):
            numberRects.append(numberSurfs[i].get_rect())
            numberRects[i].centerx = WINDOWWIDTH / 2
            numberRects[i].centery = WINDOWHEIGHT / 2
            DISPLAYSURF.blit(numberSurfs[i], numberRects[i])    
            pygame.display.update()
            pygame.time.wait(1000)
            DISPLAYSURF.fill(BGCOLOR)
        return
        
    
def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midbottom = (WINDOWWIDTH / 2, (WINDOWHEIGHT / 2) - 10)
    overRect.midtop = (WINDOWWIDTH / 2, gameRect.bottom + 20)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForQuit()

    while True:
        if checkForKeyPress():
            pygame.event.get()
            gameMode, color1, color2 = START, DEFAULTONE, DEFAULTTWO
            return gameMode, color1, color2

def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to start over.', True, WHITE)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topright = (WINDOWWIDTH - 30, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)

def checkForKeyPress():
    if len(pygame.event.get(QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE:
        terminate()
    return keyUpEvents[0].key
    

if __name__ == '__main__':
    main()
